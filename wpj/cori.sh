#!/bin/bash

set=1

python submit.py -p j0/ -f j0-knl-${set} -C knl,quad,cache -n 0.25 -q regular -w 00:29:00 -o EVENTS=0
python submit.py -p j1/ -f j1-knl-${set} -C knl,quad,cache -n 0.5 -q regular -w 00:29:00 -o EVENTS=0
python submit.py -p j2/ -f j2-knl-${set} -C knl,quad,cache -n 1 -q regular -w 00:59:00 -o EVENTS=0
python submit.py -p j3/ -f j3-knl-${set} -C knl,quad,cache -n 2 -q regular -w 00:59:00 -o EVENTS=0
python submit.py -p j4/ -f j4-knl-${set} -C knl,quad,cache -n 4 -q regular -w 02:59:00 -o EVENTS=0
python submit.py -p j5/ -f j5-knl-${set} -C knl,quad,cache -n 8 -q regular -w 05:59:00 -o EVENTS=0
python submit.py -p j6/ -f j6-knl-${set} -C knl,quad,cache -n 16 -q regular -w 05:59:00 -o EVENTS=0
cnt=0; for i in 147-157 34-42,112-122 83-98,126-129,133-136,141-146 0-33,48-58,99-111 43-47,59-82,123-125,130-132,137-140; do (( ++cnt ));
  python submit.py -p j7/ -f j7g${cnt}-hsw-${set} -n 32 -q regular -w 05:59:00 -o SPROC:=${i} -o EVENTS=0
done
cnt=0; for i in 31-34 5,6,24-27 16-20,28-30 0-4,21-23 7-15; do (( ++cnt ));
  python submit.py -p j8/ -f j8g${cnt}-hsw-${set} -n 64 -q regular -w 05:59:00 -o SPROC:=${i} -o EVENTS=0
done
cnt=0; for i in 31-34 5,6,24-27 16-20,28-30 0-4,21-23 7-15; do (( ++cnt ));
  python submit.py -p j9/ -f j9g${cnt}-hsw-${set} -n 128 -q regular -w 05:59:00 -o SPROC:=${i} -o EVENTS=0
done
