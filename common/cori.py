import sys, os, shutil, stat, optparse, itertools, inspect

def perms(s):
    if len(s) <=1:
        yield s
    else:
        for i in range(len(s)):
            for p in perms(s[:i]+s[i+1:]):
                yield s[i]+p

parser = optparse.OptionParser()
parser.add_option("-r","--resubmit",default=False,action="store_true",dest="resubmit")
parser.add_option("-w","--time",default="23:59:00",dest="time")
parser.add_option("-s","--seed",default=123456,dest="seed")
parser.add_option("-n","--nodes",default="1",dest="nodes")
parser.add_option("-N","--ncpu",default=1,dest="ncpu")
parser.add_option("-f","--file",default="run",dest="name")
parser.add_option("-p","--path",default="./",dest="path")
parser.add_option("-c","--card",default="Run.dat",dest="card")
parser.add_option("-P","--proc",default="Process",dest="proc")
parser.add_option("-o","--options",default=[],dest="opts",action="append")
parser.add_option("-b","--binary",default="/global/project/projectdirs/m1738/shoeche/sherpa/comix",dest="bin")
parser.add_option("-q","--queue",default="regular",dest="queue")
parser.add_option("-C","--core",default="haswell",dest="core")
parser.add_option("-F","--cpufac",default="0",dest="cpufac")
parser.add_option("-S","--stage",default="",dest="stage")
(opts,args) = parser.parse_args()

cwd = os.getcwd()
sys.stdout.write("Job '{0}': ".format(opts.name))
sys.stdout.flush()
if opts.resubmit:
    os.chdir('{0}/{1}'.format(cwd,opts.name))
else:
    try:
        os.mkdir(opts.name)
    except OSError:
        print "file exists, not submitted"
        sys.exit(1)
    os.chdir(opts.bin)
    os.system('git diff > {0}/{1}/source.patch'.format(cwd,opts.name))
    os.system('cp -r bin/ {0}/{1}/bin'.format(cwd,opts.name))
    os.system('cp -r lib/ {0}/{1}/lib'.format(cwd,opts.name))
    os.system('cp -r share/ {0}/{1}/share'.format(cwd,opts.name))
    os.chdir('{0}/{1}'.format(cwd,opts.name))
    with open('../'+inspect.stack()[0][1],'r') as source:
        with open('submit.py','w') as target:
            target.write('# '+' '.join(sys.argv)+'\n'+source.read())
    shutil.copyfile('../{0}'.format(opts.path+opts.card),'Run.dat')
    shutil.copytree('../{0}'.format(opts.path+opts.proc),'Process')
    try:
        shutil.copyfile('../{0}/Results.zip'.format(opts.path),'Results.zip')
    except IOError:
        pass
from math import ceil
nodes=int(ceil(float(opts.nodes)))
cores=int(float(opts.nodes)*32 if opts.core=="haswell" else float(opts.nodes)*256)/int(opts.ncpu)
if opts.stage!="":
    stage = '#DW jobdw capacity={0} access_mode=striped type=scratch\n'.format(opts.stage)
    stage += '#DW stage_in source={0}/{1} destination=$DW_JOB_STRIPED/{1} type=directory\n'.format(cwd,opts.name)
    stage += '#DW stage_out source=$DW_JOB_STRIPED/{1} destination={0}/{1} type=directory\n'.format(cwd,opts.name)
    stage += 'cd $DW_JOB_STRIPED/{0}\n'.format(opts.name)
    cmd = 'export LD_LIBRARY_PATH=$DW_JOB_STRIPED/{0}/lib/SHERPA-MC:$LD_LIBRARY_PATH\n'.format(opts.name)
else:
    stage = ''
    cmd = 'export LD_LIBRARY_PATH={0}/{1}/lib/SHERPA-MC:$LD_LIBRARY_PATH\n'.format(cwd,opts.name)
cmd += 'srun -n {2}'.format(cwd,opts.name,cores)
if opts.core!="haswell": cmd += ' -c {0} --cpu-bind=cores'.format(opts.ncpu)
cmd += ' ./bin/Sherpa '+' '.join(opts.opts)
if opts.cpufac!="0": cmd += ' CPUFAC:={0}'.format(float(opts.cpufac))
else: cmd += ' CPUFAC:={0}'.format((2 if opts.core=="haswell" else 0.25)*int(opts.ncpu))
cmd += ' -R{0} -lrun-{1}-{2}_{0}.log\n'.format(opts.seed,opts.nodes,opts.ncpu)
hints = 'export MPICH_MPIIO_DVS_MAXNODES=1\nexport MPICH_MPIIO_HINTS="*:romio_cb_write=enable:romio_ds_write=disable"\n'
with open('run.sh','w') as script:
    script.write('#!/bin/bash -l\n'+stage+hints+cmd)
cmd = "sbatch -C {6} -q {4} -N {3} -t {5} run.sh".format(cwd,opts.name,opts.seed,nodes,opts.queue,opts.time,opts.core)
os.system(cmd)
