import sys, os, shutil, stat, optparse, itertools, inspect

def perms(s):
    if len(s) <=1:
        yield s
    else:
        for i in range(len(s)):
            for p in perms(s[:i]+s[i+1:]):
                yield s[i]+p

parser = optparse.OptionParser()
parser.add_option("-r","--resubmit",default=False,action="store_true",dest="resubmit")
parser.add_option("-w","--time",default="5:59:00",dest="time")
parser.add_option("-s","--seed",default=123456,dest="seed")
parser.add_option("-n","--nodes",default="1",dest="nodes")
parser.add_option("-N","--ncpu",default=1,dest="ncpu")
parser.add_option("-f","--file",default="run",dest="name")
parser.add_option("-p","--path",default="./",dest="path")
parser.add_option("-c","--card",default="Run.dat",dest="card")
parser.add_option("-P","--proc",default="Process",dest="proc")
parser.add_option("-R","--results",default="Results.zip",dest="res")
parser.add_option("-q","--queue",default="cpu_gce",dest="queue")
parser.add_option("-o","--options",default=[],dest="opts",action="append")
parser.add_option("-b","--binary",default="/wclustre/sherpa/shoeche/sherpa/comix",dest="bin")
parser.add_option("-B","--executable",default="Sherpa",dest="exe")
(opts,args) = parser.parse_args()

cwd = os.getcwd()
sys.stdout.write("Job '{0}': ".format(opts.name))
sys.stdout.flush()
if opts.resubmit:
    os.chdir('{0}/{1}'.format(cwd,opts.name))
else:
    try:
        os.mkdir(opts.name)
    except OSError:
        print "file exists, not submitted"
        sys.exit(1)
    os.chdir(opts.bin)
    os.system('git diff > {0}/{1}/source.patch'.format(cwd,opts.name))
    os.system('cp -r bin/ {0}/{1}/bin'.format(cwd,opts.name))
    os.system('cp -r lib/ {0}/{1}/lib'.format(cwd,opts.name))
    os.system('cp -r share/ {0}/{1}/share'.format(cwd,opts.name))
    os.chdir('{0}/{1}'.format(cwd,opts.name))
    with open('../'+inspect.stack()[0][1],'r') as source:
        with open('submit.py','w') as target:
            target.write('# '+' '.join(sys.argv)+'\n'+source.read())
    shutil.copyfile('../{0}'.format(opts.path+opts.card),'Run.dat')
    shutil.copytree('../{0}'.format(opts.path+opts.proc),'Process')
    try:
        shutil.copyfile('../{0}/{1}'.format(opts.path,opts.res),'Results.zip')
    except IOError:
        pass
from math import ceil
nodes=int(ceil(float(opts.nodes)))
cores=int(float(opts.nodes)*16 if opts.queue=="cpu_gce" else float(opts.nodes)*16)/int(opts.ncpu)
cmd = 'export LD_LIBRARY_PATH={0}/{1}/lib/SHERPA-MC:$LD_LIBRARY_PATH\n'.format(cwd,opts.name)
cmd += 'mpirun -n {2} ./bin/{1} '.format(cwd,opts.exe,cores)+' '.join(opts.opts)
cmd += ' -R{0} -lrun-{1}-{2}_{0}.log\n'.format(opts.seed,opts.nodes,opts.ncpu)
with open('run.sh','w') as script:
    script.write('#!/bin/bash --norc\n'+cmd)
cmd = "sbatch -p {4} -N {3} -t {5} run.sh".format \
    (cwd,opts.name,opts.seed,opts.nodes,opts.queue,opts.time)
os.system(cmd)
