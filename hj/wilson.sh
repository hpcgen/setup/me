#!/bin/bash

set=1

# resubmit usage: './wilson.sh <path> <start job> <num jobs>'
# number of jobs: Calc 'pow(3/4,n)*(1<<n)-1'
if ! test -z "$1" && ! test -z "$2" && ! test -z "$3"; then
  cnt=$2
  for i in $(seq $(( ($2+1)*4096 )) 4096 $(( ($2+$3+1)*4096 ))); do
    $(head -n1 $1/submit.py | sed -e 's/#/python/g;s/-p/-r -s'${i}' -p/g;s/HDF5.\(\w*\). /HDF5[\1_'${cnt}'] /g');
    (( ++ cnt ));
  done;
  exit 0
fi

python submit.py -p j0/ -f j0-${set} -n 1 -o EVENTS=400000 -o EVENT_OUTPUT=HDF5[j0] -o EVENT_DISPLAY_INTERVAL=40000
python submit.py -p j1/ -f j1-${set} -n 2 -o EVENTS=100000 -o EVENT_OUTPUT=HDF5[j1] -o EVENT_DISPLAY_INTERVAL=10000
python submit.py -p j2/ -f j2-${set} -n 4 -o EVENTS=40000 -o EVENT_OUTPUT=HDF5[j2] -o EVENT_DISPLAY_INTERVAL=4000
python submit.py -p j3/ -f j3-${set} -n 8 -o EVENTS=10000 -o EVENT_OUTPUT=HDF5[j3] -o EVENT_DISPLAY_INTERVAL=1000

python submit.py -p l0/ -f l0-${set} -n 1 -o EVENTS=100000 -o EVENT_OUTPUT=HDF5[l0] -o EVENT_DISPLAY_INTERVAL=10000
python submit.py -p l1/ -f l1-${set} -n 2 -o EVENTS=10000 -o EVENT_OUTPUT=HDF5[l1] -o EVENT_DISPLAY_INTERVAL=1000

HDFOPTS="-o EVENT_GENERATION_MODE=W -o HDF5_UNWEIGHT=10"

python submit.py -p j4/ -f j4-${set} -n 16 ${HDFOPTS} -o EVENTS=4000000 -o EVENT_OUTPUT=HDF5[j4] -o EVENT_DISPLAY_INTERVAL=40000
python submit.py -p j5/ -f j5-${set} -n 32 ${HDFOPTS} -o EVENTS=1000000 -o EVENT_OUTPUT=HDF5[j5] -o EVENT_DISPLAY_INTERVAL=10000
python submit.py -p j6/ -f j6-${set} -n 32 ${HDFOPTS} -o EVENTS=100000 -o EVENT_OUTPUT=HDF5[j6] -o EVENT_DISPLAY_INTERVAL=1000

python submit.py -p l2/ -f l2-${set} -n 8 ${HDFOPTS} -o EVENTS=400000 -o EVENT_OUTPUT=HDF5[l2] -o EVENT_DISPLAY_INTERVAL=10000
