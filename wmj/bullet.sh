#!/bin/bash

set=1

# resubmit usage: './bullet <path> <start job> <num jobs>'
# number of jobs: Calc 'pow(3/4,n)*(1<<n)-1'
if ! test -z "$1" && ! test -z "$2" && ! test -z "$3"; then
  cnt=$2
  for i in $(seq $(( ($2+1)*4096 )) 4096 $(( ($2+$3+1)*4096 ))); do
    $(head -n1 $1/submit.py | sed -e 's/#/python/g;s/-p/-r -s'${i}' -p/g;s/HDF5.\(\w*\). /HDF5[\1_'${cnt}'] /g');
    (( ++ cnt ));
  done;
  exit 0
fi

python submit.py -p j0/ -f j0-${set} -n 1 -o EVENTS=409600 -o EVENT_OUTPUT=HDF5[j0] -o EVENT_DISPLAY_INTERVAL=50000 
python submit.py -p j1/ -f j1-${set} -n 1 -o EVENTS=204800 -o EVENT_OUTPUT=HDF5[j1] -o EVENT_DISPLAY_INTERVAL=10000 
python submit.py -p j2/ -f j2-${set} -n 2 -o EVENTS=51200 -o EVENT_OUTPUT=HDF5[j2] -o EVENT_DISPLAY_INTERVAL=5000
python submit.py -p j3/ -f j3-${set} -n 2 -o EVENTS=25600 -o EVENT_OUTPUT=HDF5[j3] -o EVENT_DISPLAY_INTERVAL=1000
python submit.py -p j4/ -f j4-${set} -n 4 -o EVENTS=6400 -o EVENT_OUTPUT=HDF5[j4] -o EVENT_DISPLAY_INTERVAL=500
python submit.py -p j5/ -f j5-${set} -n 8 -o EVENTS=1600 -o EVENT_OUTPUT=HDF5[j5] -o EVENT_DISPLAY_INTERVAL=100
python submit.py -p j6/ -f j6-${set} -n 16 -o EVENTS=400 -o EVENT_OUTPUT=HDF5[j6] -o EVENT_DISPLAY_INTERVAL=50
cnt=0; for i in 147-157 55-70,106-109 90-98,119-129,135-138,141-146 0-54 71-89,99-105,110-118,130-134,139,140; do (( ++cnt ));
  ## number of jobs: 2 8 2 4 1
  python submit.py -p j7/ -f j7g${cnt}-${set} -n 32 -o SPROC:=${i} -o EVENTS=100 -o EVENT_OUTPUT=HDF5[j7g${cnt}] -o EVENT_DISPLAY_INTERVAL=10
  cp j7/g${cnt}/Results.zip j7g${cnt}-${set}/
done
cnt=0; for i in 31-34 12-16,22 19,20,25-30 1,2,4-7,10,11,21 0,3,8,9,17,18,23,24; do (( ++cnt ));
  ## number of jobs: 2 15 2 2 4
  python submit.py -p j8/ -f j8g${cnt}-${set} -n 64 -o SPROC:=${i} -o EVENTS=25 -o EVENT_OUTPUT=HDF5[j8g${cnt}] -o EVENT_DISPLAY_INTERVAL=5
  cp j8/g${cnt}/Results.zip j8g${cnt}-${set}/
done
cnt=0; for i in 31-34 12-16,22 19,20,25-30 1,2,4-7,10,11,21 0,3,8,9,17,18,23,24; do (( ++cnt ));
  ## number of jobs: 2 8 1 1 3
  python submit.py -p j9/ -f j9g${cnt}-${set} -n 128 -o SPROC:=${i} -o EVENTS=5 -o EVENT_OUTPUT=HDF5[j9g${cnt}] -o EVENT_DISPLAY_INTERVAL=5
  cp j9/g${cnt}/Results.zip j9g${cnt}-${set}/
done
