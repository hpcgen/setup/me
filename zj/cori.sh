#!/bin/bash

set=1

python submit.py -p j0/ -f j0-knl-${set} -C knl,quad,cache -n 0.25 -q regular -w 00:29:00 -o EVENTS=0
python submit.py -p j1/ -f j1-knl-${set} -C knl,quad,cache -n 0.5 -q regular -w 00:29:00 -o EVENTS=0
python submit.py -p j2/ -f j2-knl-${set} -C knl,quad,cache -n 1 -q regular -w 00:59:00 -o EVENTS=0
python submit.py -p j3/ -f j3-knl-${set} -C knl,quad,cache -n 2 -q regular -w 00:59:00 -o EVENTS=0
python submit.py -p j4/ -f j4-knl-${set} -C knl,quad,cache -n 4 -q regular -w 02:59:00 -o EVENTS=0
python submit.py -p j5/ -f j5-knl-${set} -C knl,quad,cache -n 8 -q regular -w 05:59:00 -o EVENTS=0
python submit.py -p j6/ -f j6-hsw-${set} -n 16 -q regular -w 23:59:00 -o EVENTS=0
cnt=0; for i in 160-180 69-81,113-124 37-49,139-150 4-16,87-99 0-3,50-68,82-86,100-112 17-36,125-138,151-159; do (( ++cnt ));
  python submit.py -p j7/ -f j7g${cnt}-hsw-${set} -n 32 -q regular -w 05:59:00 -o SPROC:=${i} -o EVENTS=0
done
cnt=0; for i in 40-47 9-12,17-20 29-32,36-39 1-4,22-25 5-8,13-16 0,21,26-28,33-35; do (( ++cnt ));
  python submit.py -p j8/ -f j8g${cnt}-hsw-${set} -n 64 -q regular -w 05:59:00 -o SPROC:=${i} -o EVENTS=0
done
cnt=0; for i in 47,43 40-42 44-46 9-12 17-20 29-32 36-39 1-4 22-25 0,5-8 13-16 21,26-28 33-35; do (( ++cnt ));
  python submit.py -p j9/ -f j9g${cnt}-hsw-${set} -n 128 -q regular -w 11:59:00 -o SPROC:=${i} -o EVENTS=0
done
